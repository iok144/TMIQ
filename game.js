"use strict"

const SPRITE_SIZE = 48;
const COLS = 20;
const ROWS = 13;
const HERO = 0;

var phaserGame = new Phaser.Game(COLS * SPRITE_SIZE, ROWS * SPRITE_SIZE, Phaser.AUTO, null, {
    create: create,
    preload: preload,
    update: update
});

class Game {
    constructor(cols, rows, spriteSize) {
        this.actors = [];
        this.map = new Map();
        this.currentActor = 0;
    }

    updateActors() {
        for (var i = 1; i < this.actors.length; i++) {
            this.actors[i].AI();
        }
    }

    getActor(x, y) {
        for (var i = 0; i < this.actors.length; i++) {
            if (this.actors[i].x == x && this.actors[i].y == y) {
                return this.actors[i];
            }
        }
        return null;
    }

    addActor(x, y, name, spriteName) {
        game.actors.push(new Actor(x, y, name, spriteName));
        let actorsLen = game.actors.length - 1;
        game.actors[actorsLen].sprite = phaserGame.add.sprite(game.actors[actorsLen].x * SPRITE_SIZE, game.actors[actorsLen].y * SPRITE_SIZE, game.actors[actorsLen].spriteName);
        game.actors[actorsLen].sprite.bringToTop();
    }

    isInBorders(x, y) {
        return (x < COLS && x >= 0 && y < ROWS && y >= 0);
    }
}


function preload() {
    phaserGame.load.image('grass', 'assets/grass.png');
    phaserGame.load.image('grass1', 'assets/grass1.png');
    phaserGame.load.image('grass2', 'assets/grass2.png');
    phaserGame.load.image('grass3', 'assets/grass3.png');
    phaserGame.load.image('grass4', 'assets/grass4.png');

    phaserGame.load.image('hero', 'assets/player.png');
    phaserGame.load.image('slimeBlue', 'assets/slimeBlue.png');
    phaserGame.load.image('slimeRed', 'assets/slimeRed.png');
    phaserGame.load.image('stone', 'assets/stone.png');
}

var game = new Game(COLS, ROWS, SPRITE_SIZE);


function create() {
    // init keyboard commands
    phaserGame.input.keyboard.addCallbacks(null, null, onKeyUp);

    game.map.initMap(COLS, ROWS);

    game.addActor(0, 0, "heroName", "hero");
    game.addActor(3, 3, "Slime", "slimeBlue");
    game.addActor(12, 3, "Slime", "slimeBlue");
    game.addActor(5, 1, "Slime", "slimeBlue");
    game.addActor(17, 10, "Slime", "slimeRed");
    game.addActor(7, 3, "Slime", "slimeRed");
}

function update() {

    var action = game.actors[game.currentActor].getAction();

    if (action == null) {
        game.currentActor = (game.currentActor + 1) % game.actors.length;
        return;
    }

    console.log(game.currentActor == 0 ? "Hero " : 'Slime ');
    action.perform();
    game.currentActor = (game.currentActor + 1) % game.actors.length;
}

function onKeyUp(event) {

    // act on player input
    switch (event.keyCode) {
        case Phaser.Keyboard.LEFT:
            game.actors[HERO].setNextAction(new WalkAction(-1, 0));
            break;

        case Phaser.Keyboard.RIGHT:
            game.actors[HERO].setNextAction(new WalkAction(1, 0));
            break;

        case Phaser.Keyboard.UP:
            game.actors[HERO].setNextAction(new WalkAction(0, -1));
            break;

        case Phaser.Keyboard.DOWN:
            game.actors[HERO].setNextAction(new WalkAction(0, 1));
            break;
    }

    game.updateActors();
}