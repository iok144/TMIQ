class MapObject {
    constructor(x, y, spriteName, passability) {
        this.x = x;
        this.y = y;
        this.spriteName = spriteName;
        this.passable = passability;
        this.sprite;
    }
}

class Map {
    constructor() {
        this.objects = [];
    }

    initMap(width, height) {
        // create a new random map
        for (var y = 0; y < height; y++) {
            this.objects[y] = []
                //Соре за эту "мощную" генерацию, я не хотел напрягаться
            for (var x = 0; x < width; x++) {
                if (Math.random() > 0.9)
                    this.objects[y][x] = new MapObject(x, y, "stone", false);
                else {
                    if (Math.random() > 0.72) {
                        this.objects[y][x] = new MapObject(x, y, "grass", true);
                    } else if (Math.random() > 0.54) {
                        this.objects[y][x] = new MapObject(x, y, "grass1", true);

                    } else if (Math.random() > 0.36) {
                        this.objects[y][x] = new MapObject(x, y, "grass2", true);

                    } else if (Math.random() > 0.18) {
                        this.objects[y][x] = new MapObject(x, y, "grass3", true);

                    } else if (Math.random() > 0.0) {
                        this.objects[y][x] = new MapObject(x, y, "grass4", true);
                    }
                }
                this.objects[y][x].sprite = phaserGame.add.sprite(x * SPRITE_SIZE, y * SPRITE_SIZE, this.objects[y][x].spriteName);
            }
        }

    }
}