class Actor {
    constructor(x, y, name, spriteName) {
        this.x = x;
        this.y = y;
        this.dx;
        this.dy;
        this.name = name;
        this.spriteName = spriteName;
        this.sprite;
        this.nextAction = null;
    }

    setNextAction(action) {
        this.nextAction = action;
    }

    getAction() {
        var action = this.nextAction;
        // Only perform it once.
        this.nextAction = null;
        return action;
    }

    AI() {
        //console.log("Slime x " + this.x + " y " + this.y);

        if (Math.random() > 0.5) {
            this.setNextAction(new WalkAction(1, 0));
            return;
        }
        if (Math.random() > 0.5) {
            this.setNextAction(new WalkAction(-1, 0));
            return;
        }
        if (Math.random() > 0.5) {
            this.setNextAction(new WalkAction(0, 1));
            return;
        }

        if (Math.random() > 0.5) {
            this.setNextAction(new WalkAction(0, -1));
        }
    }

    canOccupy(x, y) {
        return game.isInBorders(x, y) && game.map.objects[y][x].passable && game.getActor(x, y) === null;
    }

}