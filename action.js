class Action {
    perform() {};
}

class WalkAction extends Action {
    constructor(dx, dy) {
        super(); //Чтобы юзать this
        this.dx = dx;
        this.dy = dy;

    }
    perform() {
        let actor = game.actors[game.currentActor];
        let newX = actor.x + this.dx;
        let newY = actor.y + this.dy;
        console.log("Perform x " + newX + " y " + newY);

        if (actor.canOccupy(newX, newY)) {
            actor.x = newX;
            actor.y = newY;
            actor.sprite.x = actor.x * SPRITE_SIZE;
            actor.sprite.y = actor.y * SPRITE_SIZE;
        }
    }
}